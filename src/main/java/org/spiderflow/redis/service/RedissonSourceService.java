package org.spiderflow.redis.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.redisson.api.RedissonClient;
import org.spiderflow.redis.executor.function.RedisFunctionExecutor;
import org.spiderflow.redis.executor.function.RedissonFunctionExecutor;
import org.spiderflow.redis.mapper.RedisSourceMapper;
import org.spiderflow.redis.model.RedisSource;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.io.Serializable;

@Service
public class RedissonSourceService extends ServiceImpl<RedisSourceMapper, RedisSource>{
	
	@Override
	public boolean saveOrUpdate(RedisSource entity) {
		if(entity.getId() != null){
			removeOldRedisTemplate(entity.getId());
		}
		return super.saveOrUpdate(entity);
	}
	
	@Override
	public boolean removeById(Serializable id) {
		removeOldRedisTemplate(id);
		return super.removeById(id);
	}
	
	private void removeOldRedisTemplate(Serializable id){
		RedisSource oldSource = getById(id);
		if(oldSource != null){
			RedissonClient redisson = RedissonFunctionExecutor.REDIS_CACHED_TEMPLATE.get(oldSource.getAlias());
			if(redisson != null){
				redisson.shutdown();
			}
			RedissonFunctionExecutor.REDIS_CACHED_TEMPLATE.remove(oldSource.getAlias());
		}
	}

}
